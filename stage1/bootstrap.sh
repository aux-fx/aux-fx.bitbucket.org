#!/bin/bash

declare -r username="${1?Please provide a new username}"

sudo apt update
sudo apt upgrade
sudo apt install git

sudo adduser "${username}"
my_groups=$(groups ubuntu | cut -d ' ' -f 4- | tr ' ' ',')
sudo usermod -G "${my_groups}" "${username}"
groups "${username}"

sudo sh -c 'echo export FX_FLAVOR=unstable > /etc/profile.d/flavor.sh'

