#!/usr/bin/env bash

TEAM_LAB="${1:?Please provide the lab team id}"

# Absolute path to lab directory
DABS_LAB="${HOME}/lab"

# Create lab bench
mkdir -p "${DABS_LAB}/bench"

pushd "${DABS_LAB}"
git clone git@bitbucket.org:${TEAM_LAB}/catalog.git
git clone git@bitbucket.org:${TEAM_LAB}/stock.git
git clone git@bitbucket.org:${TEAM_LAB}/safe.git
git clone git@bitbucket.org:${TEAM_LAB}/journal.git
popd

